<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Masse de l'atmosph�re>>

  <section|Exercice>

  <space|2em>Question: quelle est la masse de l'atmosph�re terrestre ?

  <section|Solution>

  <space|2em>L'erreur a ne pas faire ici est de se lancer dans un calcul trop
  compliqu�, l'�nonc� ne donnant aucune valeur num�rique, il est important de
  ne pas aboutir � un r�sultat qui n�cessiterait de nombreuses constantes que
  l'on ne connait pas. N'oublions pas qu'en physique, on est plus int�ress�
  par une approximation num�rique que par une formule exacte mais
  abstraite<text-dots>

  Ceci �tant dit:\ 

  L'atmosph�re exerce sur la terre une force gravitationelle de module
  <math|m*g>.\ 

  Cette force est per�ue � l'altitude z�ro par un observateur comme la
  pression atmosph�rique <math|P<rsub|0>=1> bar. Rappelons qu'une pression
  n'est rien d'autre qu'une force surfacique.\ 

  En appliquant le <strong|principe fondamental de la statique> :

  <\equation*>
    m*g=P<rsub|0>S
  </equation*>

  O� <math|S> repr�sente la surface de la Terre. En introduisant <math|R> le
  rayon de la Terre on peut �crire:

  <\equation*>
    m=<frac|4\<pi\>R<rsup|2>P<rsub|0>|g>
  </equation*>

  \ En prenant les valeurs num�riques suivantes:

  <\itemize>
    <item><math|P<rsub|0>=1> bar = <math|10<rsup|5>> pascal

    <item><math|R=6400> km

    <item><math|g=9.81> ms<math|<rsup|-2>>
  </itemize>

  On obtient:

  <\equation*>
    <block|<tformat|<table|<row|<cell|m=5.24\<times\>10<rsup|18> kg>>>>>
  </equation*>

  <strong|Remarque:> si l'on ne connait pas une valeur num�rique il faut
  savoir la retrouver. Par exemple ici on peut ne pas conna�tre la rayon de
  la Terre car ce n'est pas une valeur que l'on utilise tous les jours. En
  revanche il est courant de savoir que \S on est jamais � plus de 20 000 km
  de chez soi \T <text-dots>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_4.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Exercice>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Corrig�>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>