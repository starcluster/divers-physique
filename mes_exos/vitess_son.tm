<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Vitesse du son dans l'air>>

  <section|Exercice>

  Montrer que dans l'air, la vitesse du con v�rifie:

  <\equation*>
    v=<sqrt|<frac|\<gamma\>P|\<rho\>>>
  </equation*>

  Montrer ensuite que cette expression d�pend de la temp�rature.

  <section|Solution>

  <space|2em>On consid�re ici que l'air est un gaz parfait et que durant la
  propagation de l'onde sonore l'entropie de l'air ne change pas. On peut
  alors appliquer la loi de <strong|Laplace> et �crire:

  <\equation*>
    P*V<rsup|\<gamma\>>=const
  </equation*>

  avec <math|\<gamma\>=1.4 > pour l'air.

  On en d�duit:

  <\equation*>
    P*\<rho\><rsup|-\<gamma\>>=const<rprime|'>
  </equation*>

  On d�rive cette expression par rapport � <math|\<rho\>>:

  <\equation*>
    <frac|\<partial\><around*|(|P*\<rho\><rsup|-\<gamma\>>|)>|\<partial\>\<rho\>>=<frac|\<partial\>P*|\<partial\>\<rho\>>\<rho\><rsup|-\<gamma\>>+P<frac|\<partial\>*\<rho\><rsup|-\<gamma\>>|\<partial\>\<rho\>>=<frac|\<partial\>P*|\<partial\>\<rho\>>\<rho\><rsup|-\<gamma\>>+P<frac|\<partial\>*\<rho\><rsup|-\<gamma\>>|\<partial\>\<rho\>>
  </equation*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<partial\><around*|(|P*\<rho\><rsup|-\<gamma\>>|)>|\<partial\>\<rho\>>>|<cell|=>|<cell|<frac|\<partial\>P*|\<partial\>\<rho\>>\<rho\><rsup|-\<gamma\>>+P<frac|\<partial\>*\<rho\><rsup|-\<gamma\>>|\<partial\>\<rho\>>>>|<row|<cell|>|<cell|=>|<cell|<frac|\<partial\>P*|\<partial\>\<rho\>>\<rho\><rsup|-\<gamma\>>-\<gamma\>P\<rho\><rsup|-\<gamma\>-1>>>|<row|<cell|>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  En simplifiant par <math|\<rho\><rsup|-\<gamma\>>> on obtient le r�sultat
  voulu:

  <\equation*>
    v<rsup|2>=<frac|\<partial\>P*|\<partial\>\<rho\>>=<frac|\<gamma\>P|\<rho\>>
  </equation*>

  L'�quation<strong| d'�tat des gaz parfait> permet d'�crire:

  <\equation*>
    \<rho\>=<frac|P*M|R*T>
  </equation*>

  On en d�duit:

  <\equation*>
    v=<sqrt|<frac|\<gamma\>R*T|M>>\<propto\> <sqrt|T>
  </equation*>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_4.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Exercice>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Solution>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>