<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Fonctions de Hankel>|<\doc-subtitle>
    Formulaire
  </doc-subtitle>>

  <section|Définition>

  <math|J<rsub|\<nu\>>> et <math|N<rsub|\<nu\>>> représentent les fonctions
  de Bessel de première et deuxième espèce, alors:

  <\equation*>
    H<rsub|\<nu\>><rsup|<around*|(|1|)>>=J<rsub|\<nu\>>+i*N<rsub|\<nu\>><space|2em>H<rsub|\<nu\>><rsup|<around*|(|2|)>>=J<rsub|\<nu\>>-i*N<rsub|\<nu\>>
  </equation*>

  On a alors pour <math|\<nu\>> entier:

  <\equation*>
    H<rsub|-\<nu\>><rsup|<around*|(|1|)>><around*|(|x|)>=<around*|(|-1|)><rsup|m>H<rsub|\<nu\>><rsup|<around*|(|1|)>><around*|(|x|)>
  </equation*>

  <section|Représentation intégrale>

  Pour <math|x\<gtr\>0,><math|-1\<less\>\<Re\><around*|(|\<nu\>|)>\<less\>1>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around*|(|1|)>><around*|(|x|)>=<frac|\<mathe\><rsup|-<frac|\<nu\>\<pi\>\<imath\>|2>>|\<pi\>i><big|int><rsub|-\<infty\>><rsup|\<infty\>>\<mathe\><rsup|i*x
    ch t-\<nu\><rsub|>t>\<mathd\>t=<frac|2\<mathe\><rsup|-<frac|\<nu\>\<pi\>\<imath\>|2>>|\<pi\>i><big|int><rsub|0><rsup|\<infty\>>\<mathe\><rsup|i*x
    ch t> ch \<nu\>t \<mathd\>t
  </equation*>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around*|(|2|)>><around*|(|x|)>=-<frac|\<mathe\><rsup|<frac|\<nu\>\<pi\>\<imath\>|2>>|\<pi\>i><big|int><rsub|-\<infty\>><rsup|\<infty\>>\<mathe\><rsup|-i*x
    ch t-\<nu\><rsub|>t>\<mathd\>t=-<frac|2\<mathe\><rsup|<frac|\<nu\>\<pi\>\<imath\>|2>>|\<pi\>i><big|int><rsub|0><rsup|\<infty\>>\<mathe\><rsup|-i*x
    ch t> ch \<nu\>t \<mathd\>t
  </equation*>

  Pour <math|\<Re\><around*|(|z|)>\<gtr\>0,\<Re\><around*|(|\<nu\>|)>\<gtr\>-<frac|1|2>>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around|(|1|)>><around|(|z|)>=-<frac|2<rsup|\<nu\>+1>*i*z<rsup|\<nu\>>|\<Gamma\>*<around*|(|\<nu\>+<frac|1|2>|)>*\<Gamma\><around*|(|<frac|1|2>|)>>*<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|cos<rsup|\<nu\>-<frac|1|2>>t*e<rsup|i*<around*|(|z-\<nu\>*t+<frac|t|2>|)>>|sin<rsup|2*\<nu\>+1>t>*exp
    <around*|(|-2*z<math-up|ctg><around*|(|t|)>*|)>\<mathd\>*t
  </equation*>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around|(|2|)>><around|(|z|)>=-<frac|2<rsup|\<nu\>+1>*i*z<rsup|\<nu\>>|\<Gamma\>*<around*|(|\<nu\>+<frac|1|2>|)>*\<Gamma\><around*|(|<frac|1|2>|)>>*<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|cos<rsup|\<nu\>-<frac|1|2>>t*e<rsup|-i*<around*|(|z-\<nu\>*t+<frac|t|2>|)>>|sin<rsup|2*\<nu\>+1>t>*exp
    <around*|(|-2*z<math-up|ctg><around*|(|t|)>*|)>\<mathd\>*t
  </equation*>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around|(|1|)>><around|(|x|)>=-<frac|2*i<around*|(|<frac|x|2>|)><rsup|-\<nu\>>|<sqrt|\<pi\>>*\<Gamma\>*<around*|(|<frac|1|2>-\<nu\>|)>>*<big|int><rsub|1><rsup|\<infty\>><frac|e<rsup|i*x*t>|<around*|(|t<rsup|2>-1|)><rsup|\<nu\>+<frac|1|2>>>*d*t
  </equation*>

  <\equation*>
    H<rsub|\<nu\>><rsup|<around|(|2|)>><around|(|x|)>=-<frac|2*i<around*|(|<frac|x|2>|)><rsup|-\<nu\>>|<sqrt|\<pi\>>*\<Gamma\>*<around*|(|<frac|1|2>-\<nu\>|)>>*<big|int><rsub|1><rsup|\<infty\>><frac|e<rsup|-i*x*t>|<around*|(|t<rsup|2>-1|)><rsup|\<nu\>+<frac|1|2>>>*d*t
  </equation*>

  <section|Relations de récurrence>

  <\equation*>
    H<rsub|\<nu\>+1><around*|(|x|)>+H<rsub|\<nu\>-1><around*|(|x|)>=<frac|2H<rsub|\<nu\>><around*|(|x|)>|x>
  </equation*>

  <\equation*>
    H<rsub|\<nu\>+1><around*|(|x|)>-H<rsub|\<nu\>-1><around*|(|x|)>=2<frac|\<mathd\>H<rsub|\<nu\>><around*|(|x|)>|\<mathd\>x>
  </equation*>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_3.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Définition>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Représentation
      intégrale> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>